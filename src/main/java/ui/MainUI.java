package ui;

import controller.MainUIController;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

import java.io.File;
import java.io.IOException;
import java.net.URL;

public class MainUI extends Application {

    private Stage primaryStage;
    private Parent rootLayout;
    private MainUIController controller;

    @Override
    public void start(Stage primaryStage) throws Exception{
        this.primaryStage = primaryStage;
        this.primaryStage.setTitle("Writing shifts to Google Calendar");

        initRootLayout();
    }

    private void initRootLayout() throws IOException {
        FXMLLoader loader = new FXMLLoader(getClass().getResource("MainUI.fxml"));
        rootLayout = loader.load();
        primaryStage.setScene(new Scene(rootLayout));
        primaryStage.show();
        controller = loader.getController();
    }


    public static void main(String[] args) {
        launch(args);
    }
}
