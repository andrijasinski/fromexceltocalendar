package controller;

import javafx.collections.FXCollections;
import javafx.fxml.FXML;
import javafx.scene.control.*;


public class MainUIController {

    @FXML
    private Button fileSelectButton;

    @FXML
    private Button fileSelectConfirm;

    @FXML
    private ChoiceBox<String> sheetsBox;

    @FXML
    private Button sheetConfirm;

    @FXML
    private TextField name;

    @FXML
    private Button nameConfirm;

    @FXML
    private Button writeConfirm;


    public MainUIController() {
    }


    @FXML
    private void initialize() {

        sheetsBox.setItems(FXCollections.observableArrayList("Test lol"));

    }
}
