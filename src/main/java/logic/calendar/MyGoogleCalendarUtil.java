package logic.calendar;

import logic.apachepoi.Shift;
import com.google.api.client.util.DateTime;
import com.google.api.services.calendar.Calendar;
import com.google.api.services.calendar.model.Event;
import com.google.api.services.calendar.model.EventDateTime;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.core.Logger;

import java.io.IOException;
import java.util.List;

public class MyGoogleCalendarUtil {

    private static final Logger log = (Logger) LogManager.getLogger(MyGoogleCalendarUtil.class);


    private static List<Event> items;
    private static Calendar service;
    private static String workerName;

    public static void init(MyGoogleCalendar mgc, String name){
        service = mgc.getService();
        items = mgc.getItems();
        workerName = name;
        log.info("MyGoogleCalendarUtil initialized");
    }

    public static void addShift(MyGoogleCalendar mgc, Shift shift) throws IOException {
        for (Event item : items) {
            String summary = item.getSummary();
            DateTime dateTime = item.getStart().getDateTime();
            if (dateTime == null) {
                dateTime = item.getStart().getDate();
            }
            String[] s = dateTime.toString().split("T");

            if (summary.equals("Work at Apollo")){
                java.util.Calendar start = shift.getStart();
                int i = start.get(java.util.Calendar.DAY_OF_MONTH);
                String[] split = s[0].split("-");
                if (i == Integer.parseInt(split[2]) && (start.get(java.util.Calendar.MONTH)+1) == (Integer.parseInt(split[1]))){
                    return;
                }
            }

        }

        log.info("Found unique shift");
        Event event = createEvent(shift);

        event.setReminders(new Event.Reminders().setUseDefault(false));

        String calendarId = "primary";
        event = service.events().insert(calendarId, event).execute();
        log.info(String.format("Event created: %s\n", event.getHtmlLink()));

    }

    private static Event createEvent(Shift s) {
        Event event = new Event()
                .setSummary("Work at Apollo")
                .setLocation("Apollo Kino Lõunakeskus")
                .setDescription("Worker: " + workerName)
                ;
        String startString = getTimeString(s, 0);
        log.info("Created START timestamp: " + startString);
        DateTime startDateTime = new DateTime(startString);
        EventDateTime start = new EventDateTime()
                .setDateTime(startDateTime);
        event.setStart(start);


        String endString = getTimeString(s, 1);
        log.info("Created END timestamp:   " + endString);

        DateTime endDateTime = new DateTime(endString);
        EventDateTime end = new EventDateTime()
                .setDateTime(endDateTime);
        event.setEnd(end);
        /*event.set("backgroundColor", "#dbadff");
        event.set("foregroundCoor", "#dbadff");
        System.out.println(event.get("foregroundCoor"));*/
        return event;
    }

    private static String getTimeString(Shift s, int i){
        java.util.Calendar shift = i == 0 ? s.getStart() : s.getEnd();

        String calendarTime = shift.get(java.util.Calendar.YEAR) + "-"
                + addZero(shift.get(java.util.Calendar.MONTH)+1+"") + "-"
                + addZero(shift.get(java.util.Calendar.DAY_OF_MONTH)+"") + "T"
                + addZero(shift.get(java.util.Calendar.HOUR_OF_DAY)+"") + ":"
                + addZero(shift.get(java.util.Calendar.MINUTE)+"") + ":00+02:00";

        return calendarTime;
    }

    private static String addZero(String s){
        s = s.length() < 2 ? "0"+s : s;
        return s;
    }
}
