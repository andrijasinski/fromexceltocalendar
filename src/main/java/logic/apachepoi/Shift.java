package logic.apachepoi;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.core.Logger;

import java.util.Arrays;
import java.util.Calendar;
import java.util.List;

public class Shift {

    private final Logger log = (Logger) LogManager.getLogger(Shift.class);

    private final List<String> months = Arrays.asList("Jan", "Feb", "Mar", "Apr", "May", "Jun",
            "Jul", "Aug", "Sep", "Oct", "Nov", "Dec");

    private Calendar start;
    private Calendar end;

    public Shift(Calendar start, Calendar end) {
        this.start = start;
        this.end = end;
        log.info("Created shift START at:\t" + dayTime(start));
        log.info("Created shift END at:\t\t" + dayTime(end));
    }

    public Calendar getStart() {
        return start;
    }

    public Calendar getEnd() {
        return end;
    }

    @Override
    public String toString() {
        return "Shift{" +
                "start=" + dayTime(start) +
                ", end=" + dayTime(end) +
                '}'+"\n";
    }

    private String dayTime(Calendar c){
        StringBuilder sb = new StringBuilder();
        sb.append(months.get(c.get(Calendar.MONTH))+", ");
        sb.append(c.get(Calendar.DAY_OF_MONTH)+", ");
        sb.append(c.get(Calendar.HOUR_OF_DAY)+":");
        sb.append(c.get(Calendar.MINUTE));
        return sb.toString();
    }
}
