package logic.apachepoi;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.core.Logger;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.*;

public class ReadingUtil {

    private static final Logger log = (Logger) LogManager.getLogger(ReadingUtil.class);

    private static final List<String> months = Arrays.asList("Jan", "Feb", "Mar", "Apr", "May", "Jun",
            "Jul", "Aug", "Sep", "Oct", "Nov", "Dec");

    public static Workbook openFile(File path) throws IOException {
        FileInputStream fis = new FileInputStream(path);
        log.info("FileInputStream initialized");
        Workbook wb = new XSSFWorkbook(fis);
        log.info("XSSFWorkbook initialized");
        return wb;
    }

    public static Workbook openFile(String path) throws IOException {
        FileInputStream fis = new FileInputStream(new File(path));
        log.info("FileInputStream initialized");
        Workbook wb = new XSSFWorkbook(fis);
        log.info("XSSFWorkbook initialized");
        return wb;
    }

    public static List<String> findWorker(Workbook wb, int sheet, String worker) {
        Sheet sh = wb.getSheetAt(sheet);
        log.info("Looking for worker, name - " + worker);
        for (Row r : sh) {
            Cell cell = r.getCell(0);
            if (cell == null) break;
            switch (cell.getCellType()){
                case Cell.CELL_TYPE_STRING:
                    String string = cell.getRichStringCellValue().getString();
                    if (string.contains(worker)){
                        log.info("Found worker at offset " + r.getRowNum() + ", " + string);
                        return Arrays.asList(String.valueOf(r.getRowNum()), string);
                    }
                default:
                    break;
            }
        }
        log.error("No such worker in XSSFWorkbook");
        return null;
    }

    public static List<Shift> getShifts(Workbook wb, int sheet, int me, String m){
        int month = months.indexOf(m);
        List<Shift> shifts = new ArrayList<Shift>();
        log.info("Looking for workshifts");
        for (Cell cell : wb.getSheetAt(sheet).getRow(me-1)) {
            switch (cell.getCellType()){
                case Cell.CELL_TYPE_NUMERIC:
                    if (DateUtil.isCellDateFormatted(cell)){

                        String[] start = simplifyDate(cell.getDateCellValue()).split(":");
                        String[] end = simplifyDate(wb.getSheetAt(sheet).getRow(me).getCell(cell.getColumnIndex()).getDateCellValue()).split(":");

                        double date = wb.getSheetAt(sheet).getRow(5).getCell(cell.getColumnIndex()).getNumericCellValue();

                        Calendar startShift = getShiftCalendar(start, date, month);
                        Calendar endShift = getShiftCalendar(end, date, month);
                        if (startShift.before(Calendar.getInstance())){
                            break;
                        }
                        shifts.add(new Shift(startShift, endShift));
                    }
                    break;
            }
        }
        log.info("Found " + shifts.size() + " shifts in " + m);
        return shifts;
    }

    private static Calendar getShiftCalendar(String[] shift, double date, int month){
        Calendar c = Calendar.getInstance();
        c.set(Calendar.MONTH, month);
        if (shift[0].equals("00")){
            c.set(Calendar.DAY_OF_MONTH, (int) date+1);
        } else {
            c.set(Calendar.DAY_OF_MONTH, (int) date);
        }
        c.set(Calendar.HOUR_OF_DAY, Integer.parseInt(shift[0]));
        c.set(Calendar.MINUTE, Integer.parseInt(shift[1]));

        return c;
    }

    private static String simplifyDate(Date d){
        SimpleDateFormat sdf = new SimpleDateFormat("HH:mm");
        return sdf.format(d);
    }

}
