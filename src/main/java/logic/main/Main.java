package logic.main;

import logic.apachepoi.Shift;
import logic.calendar.MyGoogleCalendar;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.core.Logger;
import org.apache.poi.ss.usermodel.Workbook;

import javax.swing.*;
import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;

import static logic.apachepoi.ReadingUtil.*;
import static logic.calendar.MyGoogleCalendarUtil.addShift;
import static logic.calendar.MyGoogleCalendarUtil.init;

public class Main {
    private static final Logger log = (Logger) LogManager.getLogger(Main.class);

    public static void main(String[] args) throws IOException {
        File selectedFile;

        /*JFileChooser fileChooser = new JFileChooser();
        fileChooser.setCurrentDirectory(new File(System.getProperty("user.home.")+"/Desktop"));
        int result = fileChooser.showOpenDialog(new JFrame("Select .xlsx file"));
        if (result == JFileChooser.APPROVE_OPTION) {
            selectedFile = fileChooser.getSelectedFile();
        } else return;
*/


        String file = "Lõunakeskus_graafik_ detsember_2017.xlsx";
        String worker = "Jasinski";
        int sheet = 3;
        String month = "Dec";
        log.info(String.format("Executing program with next data: %s, %s", worker, month));
        Workbook wb = openFile(file);
        List<String> workerData = findWorker(wb, sheet, worker);
        List<Shift> shifts = getShifts(wb, sheet, Integer.parseInt(workerData.get(0)), month);
        MyGoogleCalendar mgc = new MyGoogleCalendar();
        init(mgc, workerData.get(1));

        HashSet<String> hashSet = new HashSet<String>(Arrays.asList("2930394903".split("")));
        System.out.println(hashSet);
        //System.out.println(shifts.get(13));
        for (Shift shift : shifts) {
            addShift(mgc, shift);
        }
        //addShift(mgc, shifts.get(0));
    }
}


